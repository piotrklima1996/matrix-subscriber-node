/*
 * This file is part of project link.developers/ld-node-chatmessage-subscriber-2.
 * It is copyrighted by the contributors recorded in the version control history of the file,
 * available from its original location https://gitlab.com/link.developers/ld-node-chatmessage-subscriber-2.
 *
 * SPDX-License-Identifier: MPL-2.0
 */

#include "UTF8Helper.hpp"

#include <locale>
#include <codecvt>
#include <iostream>
#ifdef _WIN32
#include <io.h>
#include <fcntl.h>
#endif

// as explained here, we should use wstring on windows and string on linux
// https://stackoverflow.com/questions/402283/stdwstring-vs-stdstring
#ifdef _WIN32
#define STD_STRING std::wstring
#define STD_IN std::wcin
#define STD_OUT std::wcout
static std::wstring_convert<std::codecvt_utf8<wchar_t>> utf8_conv;
#define CONV_TO(input) utf8_conv.to_bytes(input)
#define CONV_FROM(input) utf8_conv.from_bytes(input)
#else
#define STD_STRING std::string
#define STD_IN std::cin
#define STD_OUT std::cout
#define CONV_TO(input) input
#define CONV_FROM(input) input
#endif

std::string link_dev::UTF8Helper::readUTF8StringFromConsole()
{
	STD_STRING userInput;
	STD_OUT << "Input: " << std::flush;
	getline(STD_IN, userInput);

	return CONV_TO(userInput);
}

void link_dev::UTF8Helper::setConsoleMode()
{
#ifdef _WIN32
	_setmode(_fileno(stdin), _O_U16TEXT);
	_setmode(_fileno(stdout), _O_U16TEXT);
#endif
}

void link_dev::UTF8Helper::writeUTF8StringToConsole(const std::string &string)
{
	STD_OUT << CONV_FROM(string) << std::endl;
}
