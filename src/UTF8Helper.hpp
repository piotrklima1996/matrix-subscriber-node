/*
 * This file is part of project link.developers/ld-node-chatmessage-subscriber-2.
 * It is copyrighted by the contributors recorded in the version control history of the file,
 * available from its original location https://gitlab.com/link.developers/ld-node-chatmessage-subscriber-2.
 *
 * SPDX-License-Identifier: MPL-2.0
 */

#pragma once

#include <string>

namespace link_dev {
class UTF8Helper
{
public:

	static void setConsoleMode();
	static void writeUTF8StringToConsole(const std::string & string);
	static std::string readUTF8StringFromConsole();

};

}
