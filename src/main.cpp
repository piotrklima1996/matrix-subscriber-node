/*
 * This file is part of project link.developers/matrix-subscriber-node.
 * It is copyrighted by the contributors recorded in the version control history of the file,
 * available from its original location https://gitlab.com/piotrklima1996/matrix-subscriber-node.git.
 *
 * SPDX-License-Identifier: MPL-2.0
 */

#include "UTF8Helper.hpp"
#include <DRAIVE/Link2/InputPin.hpp>
#include <DRAIVE/Link2/NodeDiscovery.hpp>
#include <DRAIVE/Link2/NodeResources.hpp>
#include <DRAIVE/Link2/OutputPin.hpp>
#include <DRAIVE/Link2/SignalHandler.hpp>
#include <iostream>
#include <GenericMatrix3D_generated.h>
#include <link_dev/Interfaces/OpenCVToGenericMatrix3D.h>
#include <link_dev/Interfaces/StdContainerToGenericMatrix3D.h>


int main(int argc, char** argv)
{    
    try {
        // load the node resources for our subscriber
        DRAIVE::Link2::NodeResources nodeResources { "l2spec:/link_dev/matrix-subscriber-node", argc, argv };

        // announce that our node is "online"
        DRAIVE::Link2::NodeDiscovery nodeDiscovery { nodeResources };

        // activate the input pin
        DRAIVE::Link2::InputPin chatInputPin { nodeDiscovery, nodeResources, "matrix-message-input" };

        // set console mode, on windows that means to use UTF-16
        link_dev::UTF8Helper::setConsoleMode();
        
        // set a callback for when data arrives
        chatInputPin.addOnDataCallback(
            "matrix-messages",
            [](const link_dev::GenericMatrix3DT& matrixData) {
                std::vector<float> newVecMat = link_dev::Interfaces::GenericMatrixToStdVector<float>(matrixData);
                for(int i = 0;i<10;++i){
                    std::string chatLine = "(" + std::to_string(newVecMat[i*2]) + ", " + std::to_string(newVecMat[i*2+1]) + ")"; 
                    link_dev::UTF8Helper::writeUTF8StringToConsole(chatLine);
                }
            });

        // create a blocking signal handler
        DRAIVE::Link2::SignalHandler signalHandler {};
        signalHandler.setReceiveSignalTimeout(-1);

        // if signal is not shutdown
        while (signalHandler.receiveSignal() != LINK2_SIGNAL_INTERRUPT)
            ;

        return 0;

    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return 1;
    }
}
