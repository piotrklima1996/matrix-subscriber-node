# matrix-subscriber-node

[![build status](https://gitlab.com/piotrklima1996/matrix-subscriber-node.git/badges/master/pipeline.svg)](https://gitlab.com/piotrklima1996/matrix-subscriber-node.git/commits/master)

## Motivation and Context

- Tell the reader what this node is, what it does and provide context.
- Why is this node useful?

## The node in action

- Show the reader what the node looks like in action.
- Include screenshots etc.

## Installation

```
conda install matrix-subscriber-node
```

## Usage example

```
matrix-subscriber-node --instance-file instance.json
```

## Specification

This node implements the following RFCs:

- 

## Contribution

Your help is very much appreciated. For more information, please see our [contribution guide](./CONTRIBUTING.md) and the [Collective Code Construction Contract](https://gitlab.com/link.developers/RFC/blob/master/001/README.md) (C4).

## Maintainers

- Piotr Klimaszewski (maintainer, original author)
